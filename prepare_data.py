"""Process the voter data and create one csv files per px.

USAGE:

    python prepare_data.py --ac 163 --booths "1,2,5,8" --output AC163-PX002.csv voterlist.csv

    python prepare_data.py --ac 163 --booths "1,2,5,8" voterlist.csv > AC163-PX002.csv

"""
import pandas as pd
import argparse
import sys

def extract_data(csv_file, ac, booths, sort_by="fm_name"):
    df = pd.read_csv(csv_file)
    required_columns = [
        "fm_name", "rln_fm_nm", 
        "House_No_En", "idcard_no", 
        "part_no", "slnoinpart"] 
    
    # Take only the rows with matching ac and booths
    df = df[(df.ac_no == ac) & (df.part_no.isin(booths))] 
    
    # Take only the required columns
    df = df[required_columns]
    
    # sort ignoring the case
    df = df.iloc[df[sort_by].str.lower().argsort()]

    return df

def parse_args():    
    p = argparse.ArgumentParser()
    p.add_argument("--booths", help="Required booths separated by comma (eg: 1,4,5)", required=True)
    p.add_argument("--ac", type=int, help="AC number (eg. 163)", required=True)
    p.add_argument("--output", help="Output file")
    p.add_argument("csv_filename", help="CSV file containg the voter data")
    args = p.parse_args()
    return args
    
def main():
    args = parse_args()
    booths = [int(b) for b in args.booths.split(",")]
    df = extract_data(args.csv_filename, args.ac, booths)
    if args.output:
        df.to_csv(args.output, index=False)
    else:
        df.to_csv(sys.stdout, index=False)
        
if __name__ == "__main__":
    main()
