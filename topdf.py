"""Script to generate voterlist PDF from csv file.

This expects the CSV file to contain only the following columns, in the same order.

    ['Name', 'Relation Name', 'House', 'Voter ID', 'Part', '#']

USAGE:

    python topdf.py --ac "AC163 - Shanti Nagar" --px "PX002 - Some School" ac163-px002-data.csv

Requirements:

Pandas
reportlab
PyPDF2
    
References:
* https://pkimber.net/howto/python/modules/reportlab/header-footer.html
* http://www.blog.pythonlibrary.org/2010/09/21/reportlab-tables-creating-tables-in-pdfs-with-python/
* http://www.hoboes.com/Mimsy/hacks/multiple-column-pdf/
"""
from __future__ import print_function
from io import open
from reportlab.platypus import BaseDocTemplate, Frame, PageTemplate, Table, TableStyle
from reportlab.lib.pagesizes import A4, inch, landscape
from reportlab.lib import colors
import logging
import re
import pandas as pd
import argparse

import sys, os
import json

from PyPDF2 import PdfFileReader, PdfFileWriter

logger = logging.getLogger(__name__)


def group(values, n):
    return [values[i:i+n] for i in range(0, len(values), n)]    

def merge_pdfs(filenames, outfile):
    writer = PdfFileWriter()

    for f in filenames:
        logging.info("reading %s", f)
        fileobj = open(f, "rb")
        pdf = PdfFileReader(fileobj)
        for i in range(pdf.numPages):
            writer.addPage(pdf.getPage(i))

    with open(outfile, "wb") as fileobj:
        writer.write(fileobj)    

def draw_string_in_corner(canvas, document, corner, text, fontname, fontsize):
    headerBottom = document.bottomMargin+document.height        
    lineLength = document.width+document.leftMargin
    text = str(text)

    canvas.setFont(fontname,fontsize)
    if corner == "TL": 
        canvas.drawString(document.leftMargin, headerBottom, text)
    elif corner == "TR":
        canvas.drawRightString(lineLength, headerBottom, text)
    elif corner == "BL":
        canvas.drawString(document.leftMargin, document.bottomMargin-3, text)
    elif corner == "BR":
        canvas.drawRightString(lineLength, document.bottomMargin-3, text)


def read_voters(filename):
    voters = [json.loads(line) for line in open(filename)]
    return sorted(voters, key=lambda v: v['name'].strip(". -"))

class PDFBuilder:
    def __init__(self, ac, px, data, title=""):
        self.ac = ac
        self.px = px
        self.data = data
        self.title = title
        self.header = ['Name', 'Relation Name', 'House', 'Voter ID', 'Part', '#']
        
    def build(self, pdf_filename):

        filenames = []
        rows_per_page = 144

        # ReportLab seems to have some performance issue when asked to build a table
        # with many rows spanning multiple pages. It was taking forever to build the
        # pdf when the polling center has too many polling booths. To avoid getting 
        # into that situation, passing only one page at a time to reportlab, and
        # concatinating separately using pyPdf.
        for i, chunk in enumerate(group(self.data, rows_per_page)):
            self.page = i
            t=Table([self.header] + list(chunk), 
                repeatRows=1,
                colWidths=[1.55*inch, 1.25*inch, 0.8*inch, 0.95*inch, 0.30*inch, 0.35*inch],        
                )
            t.setStyle(get_table_style())

            filename = pdf_filename.replace(".pdf", "") + "-{:03d}.pdf".format(i)
            doc = self.make_doc(filename)
            doc.build([t])
            print("generated", filename)
            filenames.append(filename)

        merge_pdfs(filenames, pdf_filename)
        print("created", pdf_filename)
    
    def make_doc(self, output_filename):
        doc = BaseDocTemplate(output_filename, showBoundary=0, pagesize=landscape(A4), 
            rightMargin=48,
            leftMargin=48, 
            topMargin=32,
            bottomMargin=32)

        #Two Columns
        frame1 = Frame(doc.leftMargin, doc.bottomMargin, doc.width/2-6, doc.height, id='col1')
        frame2 = Frame(doc.leftMargin+doc.width/2+6, doc.bottomMargin, doc.width/2-6, doc.height, id='col2')
        doc.addPageTemplates([PageTemplate(id='TwoCol',frames=[frame1,frame2], onPage=self.addHeader)])
        return doc

    def addHeader(self, canvas, document):
        canvas.saveState()

        page = self.page * 2 + document.page

        if document.page % 2 == 1: # odd page
            draw_string_in_corner(canvas, document, "TL", self.px, "Times-Roman", 10)
            draw_string_in_corner(canvas, document, "TR", self.ac, "Times-Bold", 10)
            draw_string_in_corner(canvas, document, "BL", self.title, "Times-Bold", 10)
            draw_string_in_corner(canvas, document, "BR", str(page), "Times-Bold", 10)
        else:
            draw_string_in_corner(canvas, document, "TR", self.px, "Times-Roman", 10)
            draw_string_in_corner(canvas, document, "TL", self.ac, "Times-Bold", 10)
            draw_string_in_corner(canvas, document, "BR", self.title, "Times-Bold", 10)
            draw_string_in_corner(canvas, document, "BL", str(page), "Times-Bold", 10)
        canvas.restoreState()


def  get_table_style():
    """prepare table styles.

    We want all the rows with female voters with a different background
    so that it is easy to spot.
    """
    styles = [
            ('LINEBELOW', (0,0), (-1,-1), .3, colors.Color(.3, .3, .3)),
            ('BOX',(0,0),(-1,-1),1.0,(0,0,0)),        
            ('ALIGN', (4, 0), (5, -1), 'RIGHT'),         
            ('BACKGROUND',(0,0),(-1, 0), colors.black),
            ('TEXTCOLOR',(0,0),(-1, 0), colors.white),
            ('FONTSIZE', (0, 0), (-1, -1), 8),
            ('TEXTFONT', (0, 0), (-1, -1), 'Terminal'),
            ('TOPPADDING', (0, 0), (-1, -1), 1),
            ('BOTTOMPADDING', (0, 0), (-1, -1), 1),
        ]

    return TableStyle(styles)

def parse_args():
    p = argparse.ArgumentParser()
    p.add_argument("--px", help="Name of the polling station", default="")
    p.add_argument("--ac", help="Name of the AC", default="")
    p.add_argument("--title", help="title of the pdf", default="")
    p.add_argument("csv_filename")
    return p.parse_args()

def read_csv(filename):
    return list(pd.read_csv(filename).to_records(index=False))

def main():
    FORMAT = "%(asctime)s [%(name)s] [%(levelname)s] %(message)s"
    logging.basicConfig(format=FORMAT, level=logging.INFO)
    
    args = parse_args()

    builder = PDFBuilder(args.ac, args.px, 
                         read_csv(args.csv_filename),
                        title=args.title)
    builder.build("a.pdf")

if __name__ == '__main__':
    main()
